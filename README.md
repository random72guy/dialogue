# Dialog

## What is it?

This project seeks to model logical arguments (or discussions) as network graphs. 

Nodes represent . Each has a confidence level, i.e. how much it is believed to be true. Some assertions are lemmas, and taken to be near-certainly true. Others' certainty is initially unknown.

Edges represent arguments in support of or against these assertions. Each is weighted according to a) the likelihood that it is true, and b) how impactful or compelling it seems to be. 

Proving an assertion is a matter of determining that the assertion has a high confidence level by linking it with high-impact, high confidence arguments based on high-confidence intermediary assertions and lemmas. It is a matter of finding a path feasible between points, subject to certain constraints on confidence level. Practically speaking, the best results are minimum paths, requiring use of shortest-paths algorithms.

A node's confidence is based on its aggregate input. Generally speaking, an assertion with more supporting arguments (and paths) is stronger than one with fewer.

Perspectives are a means of accommodating different participants' understanding of the graph. Participants may disagree on confidence levels and other metrics. 


## Why is it?

Arguments are nontrivial to resolve. Humans are bad at solving them, leading to misunderstandings and polarization. To adequately explore an issue, many arguments must be considered, in the context of other points. Each point spawns more questions that should be answered before a conclusion can be reached, much less proven. This can be modeled with a graph, and the exercise of drawing that path from premise(s) to conclusion through argument is akin to a shortest-path algorithm. Debates have multiple steps: 

1. Creating assertions (nodes).
2. Creating arguments (edges) to lead from assertions to assertions.
3. Verifying relevancy, impact, and likelihood of arguments. 
4. Finding paths (relatively) certain assertions to other conclusions. 
5. Determining likelihood of conclusions (destination nodes) based on path weight.

And these steps may need to be done iteratively.

This is hard for humans to articulate, to accurately track in-the-moment, to remember across discussions, and to distribute with others. This tool may assist that endeavor. In this way, it hopes to transform debates from unproductive declarations of personal perspective - so oft lost in weeds and personal attacks - into collaborative graph-building sessions that can precisely and clearly articulate the issues at play and the likelihoods and implications of certain conclusions. 

## Applications & Future Work

* Individuals could use this (perhaps in a mobile form factor) to track debate topics and visualize the strength of their positions. Divergences in accepted assertions, arguments, confidences, etc. could be highlighted. 
* Debates could be guided towards productivity using proven graph-building and traversal algorithms.
* Logical consequences of parties' arguments could be illustrated. 
* Debates could be saved and resumed. 
* Debates could be cataloged and shared.
* Debates could be collaboratively edited (like a wiki), especially for high-importance and high-profile topics. 
* Poor debate etiquette could be detected in real-time (exposed via actions like ad-hominem attacks, frequent jumping between graph regions). Its detriment to progress could clearly indicated, as graph state would remain relatively unimproved and the consequences of some disingenuous arguments could be modeled (for instance, disputing a method or source might immediately and visibly undermine one's own arguments that depend on those).
* NLP could automatically generate nodes & edges from participants' words. Confidence could be entered by fact-checker services (automated or otherwise). 
* All human understanding could be graphed, traversed, searched, and consulted. 