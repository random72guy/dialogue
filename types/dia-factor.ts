import { DiaSource } from "./dia-source";

export class DiaFactor {
    name: string;
    description: string;
    source: Set<DiaSource>;
}