import { DiaNode } from "./dia-node";
import { DiaEdge } from "./dia-edge";

export class DialogGraph {
    nodes: Set<DiaNode>;
    edges: Set<DiaEdge>;
}