import { DiaEdge } from "./dia-edge";

export class DiaNode {
    value: string;
    edges: Set<DiaEdge>;
    confidence: number;
}