import { DiaNode } from "./dia-node";
import { DiaFactor } from "./dia-factor";
import { DIA_POLARITY } from "./dia-polarity";

export class DiaEdge {
    polarity: DIA_POLARITY;
    strength: number;
    liklihood: number;
    factors: Set<DiaFactor>;
    value: number;
    from: DiaNode;
    to: DiaNode;
}