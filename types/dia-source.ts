export class DiaSource {
    name: string;

    /**
     * Represents how much this source is trusted, in general, based on factors like integrity and quality of work.
     */
    trustFactor: number; 
}